﻿using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Repository
{
    public class BorrowerListRepository : LibraryRepository<IBorrowerList, BorrowerList>, IBorrowerListRepository
    {
        public BorrowerListRepository(LibraryContext context) : base(context)
        {

        }

        public async Task<List<BorrowerList>> GetBorrowerDetails(int borrowerId, CancellationToken cancellationToken)
        {
            return await LibraryContext.Set<BorrowerList>().Where(x => x.BorrowerId == borrowerId && x.Return!= true).ToListAsync(cancellationToken);
        }
    }
}
