﻿using LibraryApp.Domain.Repository;
using LibraryApp.Domain.SeedWork;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Repository
{
    public abstract class LibraryRepository<TRead, TWrite> : IReadRepository<TRead>, IWriteRepository<TWrite>  where TRead : IAggregateRoot where TWrite : class, TRead
    {
        internal const string CollectionCannotBeNull = "Entities collection cannot be null";
        internal const string ContextError = "Context cannot be null";
        protected readonly LibraryContext LibraryContext;
        public IUnitOfWork UnitOfWork => LibraryContext;

        protected LibraryRepository(LibraryContext context)
        {
            LibraryContext = context ?? throw new ArgumentNullException(nameof(context), ContextError);
        }

        public virtual async Task<TRead> GetByIdAsync(long id, CancellationToken cancellationToken)
        {
            return await LibraryContext.Set<TWrite>().FindAsync(new object[] { id }, cancellationToken);
        }

        public virtual async Task<IEnumerable<TWrite>> AddAsync(IEnumerable<TWrite> entities, CancellationToken cancellationToken)
        {
            if (entities == null)
                throw new Exception(CollectionCannotBeNull);

            LibraryContext.Set<TWrite>().AddRange(entities);
            await SaveChangesAsync($"{nameof(AddAsync)}", cancellationToken);

            return entities;
        }        

        public virtual async Task<IEnumerable<TWrite>> DeleteAsync(IEnumerable<TWrite> entities, CancellationToken cancellationToken)
        {
            if (entities == null)
                throw new Exception(CollectionCannotBeNull);
            foreach (var entity in entities)
            {
                LibraryContext.Set<TWrite>().Remove(entity);
            }
            await SaveChangesAsync($"{nameof(DeleteAsync)}", cancellationToken);
            return entities;
        }

        public virtual async Task<IEnumerable<TRead>> ListAllAsync(CancellationToken cancellationToken)
        {
            return await LibraryContext.Set<TWrite>().ToListAsync(cancellationToken);
        }

        public virtual async Task<IEnumerable<TWrite>> UpdateAsync(IEnumerable<TWrite> entities, CancellationToken cancellationToken)
        {
            if (entities == null)
                throw new Exception(CollectionCannotBeNull);
            foreach (var entity in entities)
            {
                LibraryContext.Entry(entity).State = EntityState.Modified;
            }
            await SaveChangesAsync($"{nameof(UpdateAsync)}", cancellationToken);
            return entities;
        }

        internal virtual async Task<int> SaveChangesAsync(string methodName, CancellationToken cancellationToken)
        {
            try
            {
                return await LibraryContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception exception)
            {
                if (!(exception.InnerException is SqlException sqlException))
                    throw new Exception( $"Exception occurred in {methodName}.", exception);

                throw new Exception( $"Exception occurred in {methodName}.", exception);
            }
        }
    }
}
