﻿using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Repository
{
    public class BookRepository : LibraryRepository<IBook, Book>,  IBookRepository
    {
        public BookRepository(LibraryContext context) : base(context)
        {

        }

        public async Task<List<Book>> GetAllBookDetails()
        {
            try
            {
                var data = await LibraryContext.Set<Book>().Where(x => x.Copies > 0).ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
