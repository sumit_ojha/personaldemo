﻿using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Repository
{
    public class BorrowerRepository : LibraryRepository<IBorrower, Borrower>, IBorrowerRepository
    {
        public BorrowerRepository(LibraryContext context) : base(context)
        {

        }
        public async Task<Borrower> GetBorrowerById(int borrowerId, CancellationToken cancellationToken)
        {
            return await LibraryContext.Set<Borrower>().FirstOrDefaultAsync(x => x.Id == borrowerId );
        }
    }
}
