﻿using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace LibraryApp.Infrastructure
{
    public class LibraryContext : DbContext, IUnitOfWork
    {
        public LibraryContext()
        {

        }

        public LibraryContext(DbContextOptions<LibraryContext> options)
            : base(options)
        {
        }

        protected LibraryContext(DbContextOptions options)
            : base(options)
        {
        }

        public virtual DbSet<Book> Book { get; set; }

        public virtual DbSet<Borrower> Borrower { get; set; }

        public virtual DbSet<BorrowerList> BorrowerList { get; set; }

    }
}
