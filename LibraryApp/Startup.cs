using LibraryApp.Application.Builders;
using LibraryApp.Application.Builders.Interfaces;
using LibraryApp.Application.Handlers;
using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Domain.Repository;
using LibraryApp.Infrastructure;
using LibraryApp.Infrastructure.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;

namespace LibraryApp
{
    [ExcludeFromCodeCoverage]
    public class Startup : StartupBase
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            
            SetupDatabase(services);
            SetupDependencies(services);
            services.AddControllers();

            SetupSwagger(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app)
        {
            var env = app.ApplicationServices.GetService<IWebHostEnvironment>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("v1/swagger.json", "LibraryApp"); });
            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Configures and adds the Entity Framework Core database context to the service collection.
        /// </summary>
        /// <param name="services">The services.</param>
        protected internal virtual void SetupDatabase(IServiceCollection services)
        {
            //
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IBorrowerRepository, BorrowerRepository>();
            services.AddScoped<IBorrowerListRepository, BorrowerListRepository>();

            services.AddDbContext<LibraryContext>((serviceProvider, options) =>
            {
                //TODO Add the DB ConnectionString
                var connectionString = "Data Source=AUSDWM1PMTDB1.AUS.AMER.DELL.COM;Initial Catalog=TestDB;Integrated Security=False; User Id=payment_user; Password=payment_user;";

                options.UseSqlServer(connectionString);
            });
        }

        /// <summary>
        /// Setups the swagger.
        /// </summary>
        /// <param name="services">The services.</param>
        protected internal virtual void SetupSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.CustomSchemaIds(type => type.ToString());

                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "LibraryApp",
                    Version = "v1",
                    Description = "Library App",

                });
                c.AddSecurityDefinition("ApiKeyAuthentication",
                    new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "Please enter Authentication Key",
                        //Name = HeaderKeyConstants.ApiKeyName,
                        Type = SecuritySchemeType.ApiKey
                    });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement() {

                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "ApiKeyAuthentication"
                            },
                            Name = "ApiKeyAuthentication",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                    }

                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            });
        }
        /// <summary>
        /// Setups the dependencies.
        /// </summary>
        /// <param name="services">The services.</param>
        protected internal virtual void SetupDependencies(IServiceCollection services)
        {
            services.AddMemoryCache();            
            services.AddScoped<ILibraryAppResultBuilder, LibraryAppResultBuilder>();
            services.AddScoped<ILibraryAppResponseBuilder, LibraryAppResponseBuilder>();
            services.AddScoped<IGetAllDataHandler, GetAllDataHandler>();
            services.AddScoped<IBorrowBookRequestHandler, BorrowBookRequestHandler>();
            services.AddScoped<IReturnBookRequestHandler, ReturnBookRequestHandler>();
        }
    }
}
