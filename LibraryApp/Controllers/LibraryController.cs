﻿using LibraryApp.Application.Builders.Interfaces;
using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Application.Models.Requests;
using LibraryApp.Application.Models.Responses;
using LibraryApp.Application.Models.Responses.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibraryController : ControllerBase
    {

        [HttpGet]
        [Route("Get")]       
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAsync(
            [FromQuery] GetAllDataRequest payload,
            [FromServices] IGetAllDataHandler handler,
            [FromServices] ILibraryAppResultBuilder resultBuilder,
            CancellationToken cancellationToken)
        {
            
            using (var linkedSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {
               
                var Response = await handler.HandleAsync(payload, linkedSource.Token);
                return resultBuilder.BuildClientResult(Response as ILibraryResponse, Response.PaymentException);
            }

        }

        [HttpPost]
        [Route("BorrowBook")]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> BorrowBookAsync(
         [FromBody] LibraryBorrowBookRequest request,
         [FromServices] IBorrowBookRequestHandler handler,
         [FromServices] ILibraryAppResultBuilder resultBuilder,
         CancellationToken cancellationToken)
        {
            
            using (var linkedSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {
                
                var Response = await handler.HandleAsync(request, linkedSource.Token);
                return resultBuilder.BuildClientResult(Response as ILibraryResponse, Response.PaymentException);
               
            }

        }

        [HttpPost]
        [Route("ReturnBook")]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(LibraryResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ReturnBookAsync(
         [FromBody] ReturnBookRequest request,
         [FromServices] IReturnBookRequestHandler handler,
         [FromServices] ILibraryAppResultBuilder resultBuilder,
         CancellationToken cancellationToken)
        {
           
            using (var linkedSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {
                var Response = await handler.HandleAsync(request, linkedSource.Token);
                return resultBuilder.BuildClientResult(Response as ILibraryResponse, Response.PaymentException);
                
            }

        }
        
               
    }
}
