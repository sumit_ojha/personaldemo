using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;

namespace LibraryApp
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            ForceUsageOfHttpMessageHandlerInsteadOfSocketsHttpHandler();
            var logger = LoggerFactory.Create(builder => builder.AddConsole().AddFilter("Critical", LogLevel.Critical)).CreateLogger("Program");

            LogErrors(() => CreateWebHostBuilder(args).Build().Run(), logger);
        }

        internal static void LogErrors(Action action, ILogger logger)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Library App service failed to execute.");
                throw;
            }
        }

        /// <summary>
        /// Uses the old HTTP handler instead of sockets HTTP handler which is provided in dotnet
        /// core 2.1 and having SSL wildcard certificate verification bug
        /// (https://github.com/dotnet/corefx/issues/34061). Hence, we have to force dotnet core to
        /// use the old http message handler for any http communications.
        /// </summary>
        private static void ForceUsageOfHttpMessageHandlerInsteadOfSocketsHttpHandler()
        {
            AppContext.SetSwitch("System.Net.Http.UseSocketsHttpHandler", false);
        }
        /// <summary>
        /// Creates the web host builder.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var contentLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            return WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(contentLocation)
                .UseStartup<Startup>()
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.SetMinimumLevel(LogLevel.Information);
                });
        }
    }
}
