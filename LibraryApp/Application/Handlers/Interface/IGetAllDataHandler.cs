﻿using LibraryApp.Application.Models.Requests.Interface;
using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Application.Models.Responses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Handlers.Interface
{
    public interface IGetAllDataHandler : ILibraryAppRequestHandler<IGetAllDataRequest, IGetAllDataResponse>
    {
    }
}
