﻿using LibraryApp.Application.Models.Requests.Interface;
using LibraryApp.Application.Models.Response.Interface;

namespace LibraryApp.Application.Handlers.Interface
{
    public interface IReturnBookRequestHandler : ILibraryAppRequestHandler<IReturnBookRequest, IGetAllDataResponse>
    {
    }
}
