﻿using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Application.Models.Requests;
using LibraryApp.Application.Models.Requests.Interface;
using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Handlers
{
    public class ReturnBookRequestHandler : IReturnBookRequestHandler
    {
        internal readonly IBookRepository bookRepository;
        internal readonly IBorrowerListRepository borrowerListRepository;
        internal readonly IGetAllDataHandler getAllDataHandler;

        public ReturnBookRequestHandler(IGetAllDataHandler _getAllDataHandler, IBookRepository _bookRepository, IBorrowerListRepository _borrowerListRepository)
        {
            getAllDataHandler = _getAllDataHandler;
            bookRepository = _bookRepository;
            borrowerListRepository = _borrowerListRepository;
        }

        public async Task<IGetAllDataResponse> HandleAsync(IReturnBookRequest request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var bookdetials = await bookRepository.GetAllBookDetails();
            var borrowerdetials = await borrowerListRepository.GetBorrowerDetails(request.borrowerId, cancellationToken);
            
            foreach (var bookid in request.bookId)
            {
                var bookentity = new Book();
                var borrowerListentities = new BorrowerList();
                var data = borrowerdetials.Find(x => x.BorrowerId == request.borrowerId && x.BookId == bookid);
                var borrowerListargs = new BorrowerList.CreateBorrowerListArguments
                {
                    Return = true
                };
                borrowerListentities = BorrowerList.Update(data, borrowerListargs);
                await borrowerListRepository.UpdateAsync(new List<BorrowerList> { borrowerListentities }, cancellationToken);
                var bookdata = bookdetials.Find(x => x.Id == bookid);
                var bookargs = new Book.CreateBookArguments
                {
                    Copies = (bookdata.Copies + 1)
                };
                bookentity = Book.Update(bookdata, bookargs);
                await bookRepository.UpdateAsync(new List<Book> { bookentity }, new CancellationToken());
            }    
            return await getAllDataHandler.HandleAsync(new GetAllDataRequest { BorrowerId = request.borrowerId }, cancellationToken);
        }
    }
}
