﻿using LibraryApp.Application.Builders.Interfaces;
using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Application.Models.Requests.Interface;
using LibraryApp.Application.Models.Response;
using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Application.Models.Responses;
using LibraryApp.Application.Models.Responses.Interfaces;
using LibraryApp.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Handlers
{
    public class GetAllDataHandler : IGetAllDataHandler
    {

        internal readonly IBookRepository bookRepository;
        internal readonly IBorrowerListRepository borrowerListRepository;

        public GetAllDataHandler(IBookRepository _bookRepository, IBorrowerListRepository _borrowerListRepository)
        {
            bookRepository = _bookRepository;
            borrowerListRepository = _borrowerListRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IGetAllDataResponse> HandleAsync(IGetAllDataRequest request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var bookdetials = await bookRepository.GetAllBookDetails();
            var book = new List<IBookDetail>();
            foreach (var data in bookdetials)
            {
                var books = new BookDetail
                {
                    BookName = data.Name,
                    NoOfCopies = data.Copies
                };
                book.Add(books);
            }
            var borrowerdetials = await borrowerListRepository.GetBorrowerDetails(request.BorrowerId, cancellationToken);
            var borrowerbook = new List<IBorrowerDetail>();
            foreach (var data in borrowerdetials)
            {

                if (!data.Return)
                {
                    var bokid = data.BookId;
                    var bookdata = bookdetials.Find(x => x.Id == bokid);
                    var borrow = new BorrowerDetail
                    {
                        bookName = bookdata.Name
                    };
                    borrowerbook.Add(borrow);
                }
            }
            var response = new GetAllDataResponse { bookDetial = book, borrowerDetial = borrowerbook };
            return response;

        }

    }
}
