﻿using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Application.Models.Requests;
using LibraryApp.Application.Models.Requests.Interface;
using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Handlers
{
    public class BorrowBookRequestHandler : IBorrowBookRequestHandler
    {
        internal readonly IBookRepository bookRepository;
        internal readonly IBorrowerListRepository borrowerListRepository;
        internal readonly IGetAllDataHandler getAllDataHandler;
        internal readonly IBorrowerRepository borrowerRepository;

        public BorrowBookRequestHandler(IGetAllDataHandler _getAllDataHandler, IBookRepository _bookRepository, IBorrowerListRepository _borrowerListRepository, IBorrowerRepository _borrowerRepository)
        {
            getAllDataHandler = _getAllDataHandler;
            bookRepository = _bookRepository;
            borrowerListRepository = _borrowerListRepository;
            borrowerRepository = _borrowerRepository;
        }

        public async Task<IGetAllDataResponse> HandleAsync(ILibraryBorrowBookRequest request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var borrower = await borrowerRepository.GetBorrowerById(request.borrowerId, cancellationToken);
            if (borrower == null)
            {
                var data = await getAllDataHandler.HandleAsync(new GetAllDataRequest { BorrowerId = request.borrowerId }, cancellationToken);
                data.PaymentException = "User is not registered";

                return data;
            }
            var bookdetials = await bookRepository.GetAllBookDetails();
            var borrowerdetials = await borrowerListRepository.GetBorrowerDetails(request.borrowerId, cancellationToken);
            var borrowerListentities = new BorrowerList();
            var bookentity = new Book();
            if (borrowerdetials.Count < 2)
            {
                var borrowerListargs = new BorrowerList.CreateBorrowerListArguments
                {
                    BookId = request.bookId,
                    BorrowerId = request.borrowerId,
                    Return = false
                };
                borrowerListentities = BorrowerList.Create(borrowerListargs);
                var bookdata = bookdetials.Find(x => x.Id == request.bookId);
                var bookargs = new Book.CreateBookArguments
                {
                    Copies = (bookdata.Copies - 1)
                };
                bookentity = Book.Update(bookdata, bookargs);

                await borrowerListRepository.AddAsync(new List<BorrowerList> { borrowerListentities }, cancellationToken);
                await bookRepository.UpdateAsync(new List<Book> { bookentity }, new CancellationToken());
            }


            return await getAllDataHandler.HandleAsync(new GetAllDataRequest { BorrowerId = request.borrowerId }, cancellationToken);

        }

    }
}
