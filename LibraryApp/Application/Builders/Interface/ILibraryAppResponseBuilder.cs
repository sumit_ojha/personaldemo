﻿using LibraryApp.Application.Models.Responses.Interfaces;

namespace LibraryApp.Application.Builders.Interfaces
{
    /// <summary>
    /// LibraryApp Response Builder
    /// </summary>
    public interface ILibraryAppResponseBuilder
    {
        /// <summary>
        /// Builds the Error response.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        ILibraryResponse BuildErrorResponse(string message, int code);

        
    }
}
