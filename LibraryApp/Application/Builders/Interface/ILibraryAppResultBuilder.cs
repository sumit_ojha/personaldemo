﻿using Microsoft.AspNetCore.Mvc;

namespace LibraryApp.Application.Builders.Interfaces
{
    /// <summary>
    /// Interface for LibraryApp result builder
    /// </summary>
    public interface ILibraryAppResultBuilder
    {
        /// <summary>
        /// Builds the express checkout result.
        /// </summary>
        /// <typeparam name="TInput">The type of the input.</typeparam>
        /// <param name="response">The response.</param>
        /// <param name="errorCode">The error code.</param>
        /// <returns></returns>
        IActionResult BuildClientResult<TInput>(TInput response, string errorCode);
    }
}

