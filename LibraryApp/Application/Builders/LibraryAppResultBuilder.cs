﻿using LibraryApp.Application.Builders.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApp.Application.Builders
{
    /// <summary>
    /// </summary>
    /// <seealso cref="LibraryApp.Application.Builders.Interfaces.ILibraryAppResultBuilder"/>
    public class LibraryAppResultBuilder : ILibraryAppResultBuilder
    {
        /// <summary>
        /// Builds the Library App result.
        /// </summary>
        /// <typeparam name="TInput">The type of the input.</typeparam>
        /// <param name="response">The response.</param>
        /// <param name="errorCode">The error code.</param>
        /// <returns></returns>
        public IActionResult BuildClientResult<TInput>(TInput response, string errorCode)
        {
            if (response == null) return new StatusCodeResult(StatusCodes.Status500InternalServerError);

            if (!string.IsNullOrWhiteSpace(errorCode))
            {
                return new JsonResult(response) { StatusCode = StatusCodes.Status400BadRequest };
            }
            return new OkObjectResult(response);
        }
    }
}