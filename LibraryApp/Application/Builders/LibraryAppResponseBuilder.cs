﻿
using LibraryApp.Application.Models.Responses;
using LibraryApp.Application.Models.Responses.Interfaces;
using LibraryApp.Application.Builders.Interfaces;


namespace LibraryApp.Application.Builders
{
    /// <summary>
    /// LibraryApp Response Builder
    /// </summary>
    public class LibraryAppResponseBuilder : ILibraryAppResponseBuilder
    {
        /// <summary>
        /// Builds the Error response.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public ILibraryResponse BuildErrorResponse(string message, int code) => new LibraryResponse()
        {
            PaymentException = message
        };
    }
}
