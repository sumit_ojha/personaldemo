﻿using LibraryApp.Application.Models.Requests.Interface;

namespace LibraryApp.Application.Models.Requests
{
    public class ReturnBookRequest : IReturnBookRequest
    {
        public int[] bookId { get; set; }
        public int borrowerId { get; set; }
    }
}
