﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Requests.Interface
{
    public interface IGetAllDataRequest
    {
        int BorrowerId{ get; }
    }
}
