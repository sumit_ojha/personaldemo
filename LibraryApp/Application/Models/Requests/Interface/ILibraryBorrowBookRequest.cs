﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Requests.Interface
{
    public interface ILibraryBorrowBookRequest
    {
        int bookId { get; set; }
        int borrowerId { get; set; }
    }
}
