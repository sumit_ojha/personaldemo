﻿namespace LibraryApp.Application.Models.Requests.Interface
{
    public interface IReturnBookRequest
    {
        int[] bookId { get; set; }
        int borrowerId { get; set; }
    }
}
