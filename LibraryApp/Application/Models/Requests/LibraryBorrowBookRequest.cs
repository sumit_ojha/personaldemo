﻿using LibraryApp.Application.Models.Requests.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Requests
{
    public class LibraryBorrowBookRequest : ILibraryBorrowBookRequest
    {
        public int bookId { get ; set ; }
        public int borrowerId { get ; set ; }
    }
}
