﻿using LibraryApp.Application.Models.Responses.Interfaces;


namespace LibraryApp.Application.Models.Responses
{
    /// <summary>
    /// Base response for ALL type
    /// </summary>
    /// <seealso cref="ILibrary Response"/>
    public class LibraryResponse : ILibraryResponse
    {
        /// <summary>
        /// Gets the payment exception.
        /// </summary>
        /// <value>The payment exception.</value>
        public string PaymentException { get; set; }
    }
}