﻿using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Domain.Converters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Response
{
    public class GetAllDataResponse : IGetAllDataResponse
    {
        [JsonProperty("bookDetial", ItemConverterType = typeof(InterfaceTypeConverter<IBookDetail, BookDetail>))]

        public List<IBookDetail> bookDetial { get ; set; }
        [JsonProperty("borrowerDetial", ItemConverterType = typeof(InterfaceTypeConverter<IBorrowerDetail, BorrowerDetail>))]
        public List<IBorrowerDetail> borrowerDetial { get; set; }

        public string PaymentException { get; set; }
    }
}
