﻿using LibraryApp.Application.Models.Response.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Response
{
    public class BookDetail : IBookDetail
    {
        public string BookName { get; set; }
        public int NoOfCopies { get ; set ; }
    }
}
