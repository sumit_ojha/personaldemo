﻿using LibraryApp.Application.Models.Response.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Response
{
    public class BorrowerDetail : IBorrowerDetail
    {
        public string bookName { get; set; }
    }
}
