﻿

namespace LibraryApp.Application.Models.Responses.Interfaces
{
    /// <summary>
    /// Base response for ALL type
    /// </summary>
    public interface ILibraryResponse 
    {
        /// <summary>
        /// Gets the payment exception.
        /// </summary>
        /// <value>The payment exception.</value>
        string PaymentException { get; set; }
    }
}