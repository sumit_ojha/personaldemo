﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Response.Interface
{
    public interface IBookDetail
    {
        string BookName { get; set; }
        int NoOfCopies { get; set; }
    }
}
