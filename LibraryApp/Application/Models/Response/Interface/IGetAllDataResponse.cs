﻿using LibraryApp.Application.Models.Responses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Response.Interface
{
    public interface IGetAllDataResponse : ILibraryResponse
    {
       List<IBookDetail> bookDetial { get; set; }
        List<IBorrowerDetail> borrowerDetial { get; set; }
    }
}
