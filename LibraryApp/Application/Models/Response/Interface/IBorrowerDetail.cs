﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Models.Response.Interface
{
    public interface IBorrowerDetail
    {
        string bookName { get; set; }
        
    }
}
