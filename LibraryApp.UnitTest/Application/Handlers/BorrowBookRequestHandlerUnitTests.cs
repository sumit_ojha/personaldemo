﻿using LibraryApp.Application.Handlers;
using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Application.Models.Requests;
using LibraryApp.Application.Models.Response;
using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.UnitTest.Application.Handlers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("App- unit")]
    public class BorrowBookRequestHandlerUnitTests
    {
        [TestMethod]
        public async Task HandleAsyncThrowsArgumentNullExceptionWhenRequestIsNull()
        {
            var bookRepository = new Mock<IBookRepository>();
            var borrowerListRepository = new Mock<IBorrowerListRepository>();
            var borrowerRepository = new Mock<IBorrowerRepository>();
            var getAllDataHandler = new Mock<IGetAllDataHandler>();

            var handlerMock = new Mock<BorrowBookRequestHandler>(getAllDataHandler.Object, bookRepository.Object, borrowerListRepository.Object, borrowerRepository.Object)
            { CallBase = true };
            var exception = await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => handlerMock.Object.HandleAsync(null, CancellationToken.None));

            Assert.IsNotNull(exception);
            Assert.IsTrue(exception.Message.StartsWith("Value cannot be null."));
        }

        [TestMethod]
        public async Task HandleAsyncHappyPath()
        {
            
            var bookRepository = new Mock<IBookRepository>();
            var borrowerListRepository = new Mock<IBorrowerListRepository>();
            var getAllDataHandler = new Mock<IGetAllDataHandler>();
            var borrowerRepository = new Mock<IBorrowerRepository>();
            var request = new LibraryBorrowBookRequest
            {
                bookId = 2,
                borrowerId = 1
            };

            var books = new List<Book>
            {
                 new Book
                 {
                     Id = 1,
                     Name = "RD Sharma",
                     Copies = 2
                 },
                 new Book
                 {
                     Id = 2,
                     Name = "HC Verma",
                     Copies = 3
                 }
            };

            var borrowerData = new List<BorrowerList>
            {
                new BorrowerList
                {
                    Id = 1,
                    BookId = 1,
                    BorrowerId = 1,
                    Return= false
                }
            };
            var responsedata = new GetAllDataResponse()
            {
                bookDetial = new List<IBookDetail>()
                {
                    new BookDetail
                    {
                         BookName = "RD Sharma",
                         NoOfCopies = 2
                    },
                    new BookDetail
                    {
                         BookName = "HC Verma",
                         NoOfCopies = 2
                    }
                },
                borrowerDetial = new List<IBorrowerDetail>
                {
                    new BorrowerDetail
                    {
                        bookName = "RD Sharma"
                    },
                    new BorrowerDetail
                    {
                        bookName = "HC Verma"
                    }
                },
                PaymentException = null
            };
            var borower = new Borrower{ BorrowerName = "Test",Id = 1} ;
            bookRepository.Setup(x => x.GetAllBookDetails()).ReturnsAsync(books);
            borrowerRepository.Setup(x => x.GetBorrowerById(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync(borower);
            borrowerListRepository.Setup(x => x.GetBorrowerDetails(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync(borrowerData);
            //bookRepository.Setup(x => x.UpdateAsync(It.IsAny<List<Book>>(), It.IsAny<CancellationToken>())).ReturnsAsync();
            getAllDataHandler.Setup(x => x.HandleAsync(It.IsAny<GetAllDataRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(responsedata);
            var handlerMock = new Mock<BorrowBookRequestHandler>(getAllDataHandler.Object, bookRepository.Object, borrowerListRepository.Object, borrowerRepository.Object)
            { CallBase = true };
            var response = handlerMock.Object.HandleAsync(request, CancellationToken.None);

            Assert.IsNotNull(response);
        }
    }
}
