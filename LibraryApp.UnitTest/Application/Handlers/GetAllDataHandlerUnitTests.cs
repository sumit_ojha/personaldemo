﻿using LibraryApp.Application.Handlers;
using LibraryApp.Application.Models.Requests;
using LibraryApp.Domain.Models.Entities;
using LibraryApp.Domain.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.UnitTest.Application.Handlers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("App- unit")]
    public class GetAllDataHandlerUnitTests
    {
        [TestMethod]
        public async Task HandleAsyncThrowsArgumentNullExceptionWhenRequestIsNull()
        {
            var bookRepository = new Mock<IBookRepository>();
            var borrowerListRepository = new Mock<IBorrowerListRepository>();
           
            var handlerMock = new Mock<GetAllDataHandler>(bookRepository.Object, borrowerListRepository.Object)
            { CallBase = true };
            var exception = await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => handlerMock.Object.HandleAsync(null, CancellationToken.None));

            Assert.IsNotNull(exception);
            Assert.IsTrue(exception.Message.StartsWith("Value cannot be null."));
        }

        [TestMethod]
        public async Task HandleAsyncHappyPath()
        {

            var bookRepository = new Mock<IBookRepository>();
            var borrowerListRepository = new Mock<IBorrowerListRepository>();
           
            
            var request = new GetAllDataRequest
            {
                BorrowerId = 1
            };

            var books = new List<Book>
            {
                 new Book
                 {
                     Id = 1,
                     Name = "RD Sharma",
                     Copies = 2
                 },
                 new Book
                 {
                     Id = 2,
                     Name = "HC Verma",
                     Copies = 3
                 }
            };

            var borrowerData = new List<BorrowerList>
            {
                new BorrowerList
                {
                    Id = 1,
                    BookId = 1,
                    BorrowerId = 1,
                    Return= false
                }
            };
           
           
            bookRepository.Setup(x => x.GetAllBookDetails()).ReturnsAsync(books);
            
            borrowerListRepository.Setup(x => x.GetBorrowerDetails(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync(borrowerData);
           
           var handlerMock = new Mock<GetAllDataHandler>(bookRepository.Object, borrowerListRepository.Object)
            { CallBase = true };
            var response = handlerMock.Object.HandleAsync(request, CancellationToken.None);

            Assert.IsNotNull(response);
        }
    }
}
