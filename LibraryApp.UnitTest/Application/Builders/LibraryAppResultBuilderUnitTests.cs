﻿using LibraryApp.Application.Builders;
using LibraryApp.Application.Models.Response.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace LibraryApp.UnitTest.Application.Builders
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("unit")]
    public class LibraryAppResultBuilderUnitTests
    {
        [TestMethod]
        public void BuildLibraryAppResultBuilderReturnsInternalServerErrorWhenRegisterResponseIsNull()
        {
            var resultBuilder = new LibraryAppResultBuilder();

            var result = resultBuilder.BuildClientResult<IGetAllDataResponse>(null, null);

            Assert.IsNotNull(result);
            Assert.AreEqual(500, ((StatusCodeResult)result).StatusCode);
        }

        [TestMethod]
        public void LibraryAppResultBuilderReturnsResponseWhenRegisterResponseIsNotHavingPaymentExceptionInitialized()
        {
            var resultBuilder = new LibraryAppResultBuilder();
            var registerResponseMock = new Mock<IGetAllDataResponse>();

            var response = resultBuilder.BuildClientResult<IGetAllDataResponse>(registerResponseMock.Object, null);

            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(OkObjectResult));
            Assert.IsInstanceOfType((response as OkObjectResult).Value, typeof(IGetAllDataResponse));
        }

        [TestMethod]
        public void LibraryAppResultBuilderResponseWhenErrorCodeIsNotNull()
        {
            var resultBuilder = new LibraryAppResultBuilder();
            var registerResponseMock = new Mock<IGetAllDataResponse>();

            var response = resultBuilder.BuildClientResult<IGetAllDataResponse>(registerResponseMock.Object, "errorcode");

            Assert.IsNotNull(response);

        }
    }
}
