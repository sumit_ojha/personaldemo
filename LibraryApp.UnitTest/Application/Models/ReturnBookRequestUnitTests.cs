﻿using LibraryApp.Application.Models.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace LibraryApp.UnitTest.Application.Models
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("LibraryApp-Unit")]
    public class ReturnBookRequestUnitTests
    {
        /// <summary>
        /// Get Returns Set Value
        /// </summary>
        [TestMethod]
        public void GetReturnsSetValue()
        {
            int[] data = new int[] { 1 };
            var request = new ReturnBookRequest
            {
               bookId=  data,
               borrowerId = 1
            };
            var serializeStr = JsonConvert.SerializeObject(request, Formatting.Indented);

            var deserializedObj = JsonConvert.DeserializeObject<ReturnBookRequest>(serializeStr);

            Assert.IsNotNull(request);
            Assert.IsNotNull(deserializedObj);
            Assert.IsInstanceOfType(deserializedObj, typeof(ReturnBookRequest));
            Assert.AreEqual(request.bookId[0], deserializedObj.bookId[0]);
            Assert.AreEqual(request.borrowerId, deserializedObj.borrowerId);
        }
    }
}
