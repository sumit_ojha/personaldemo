﻿using LibraryApp.Application.Models.Response;
using LibraryApp.Application.Models.Response.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace LibraryApp.UnitTest.Application.Models
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("LibraryApp-Unit")]
    public class GetAllDataResponseUnitTests
    {
        /// <summary>
        /// Get Returns Set Value
        /// </summary>
        [TestMethod]
        public void GetReturnsSetValue()
        {
            var request = new GetAllDataResponse
            {
                bookDetial = new List<IBookDetail>
                {
                    new BookDetail
                    {
                        BookName = "RD Sharma",
                        NoOfCopies = 1
                    }
                },
                borrowerDetial = new List<IBorrowerDetail>
                {
                    new BorrowerDetail
                    {
                        bookName = "RD Sharma"
                    }
                }
            };
            var serializeStr = JsonConvert.SerializeObject(request, Formatting.Indented);

            var deserializedObj = JsonConvert.DeserializeObject<GetAllDataResponse>(serializeStr);

            Assert.IsNotNull(request);
            Assert.IsNotNull(deserializedObj);
            Assert.IsInstanceOfType(deserializedObj, typeof(GetAllDataResponse));
            Assert.AreEqual(request.bookDetial[0].BookName, deserializedObj.bookDetial[0].BookName);
            Assert.AreEqual(request.bookDetial[0].NoOfCopies, deserializedObj.bookDetial[0].NoOfCopies);
            Assert.AreEqual(request.borrowerDetial[0].bookName, deserializedObj.borrowerDetial[0].bookName);
        }
    }
}
