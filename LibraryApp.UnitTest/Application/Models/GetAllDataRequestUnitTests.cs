﻿using LibraryApp.Application.Models.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace LibraryApp.UnitTest.Application.Models
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("LibraryApp-Unit")]
    public class GetAllDataRequestUnitTests
    {
        /// <summary>
        /// Get Returns Set Value
        /// </summary>
        [TestMethod]
        public void GetReturnsSetValue()
        {
            var request = new GetAllDataRequest
            {
                BorrowerId = 1
            };
            var serializeStr = JsonConvert.SerializeObject(request, Formatting.Indented);

            var deserializedObj = JsonConvert.DeserializeObject<GetAllDataRequest>(serializeStr);

            Assert.IsNotNull(request);
            Assert.IsNotNull(deserializedObj);
            Assert.IsInstanceOfType(deserializedObj, typeof(GetAllDataRequest));
            Assert.AreEqual(request.BorrowerId, deserializedObj.BorrowerId);
           
        }
    }
}
