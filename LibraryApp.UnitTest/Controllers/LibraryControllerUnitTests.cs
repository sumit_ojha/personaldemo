﻿using LibraryApp.Application.Builders.Interfaces;
using LibraryApp.Application.Handlers.Interface;
using LibraryApp.Application.Models.Requests;
using LibraryApp.Application.Models.Response.Interface;
using LibraryApp.Application.Models.Responses.Interfaces;
using LibraryApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.UnitTest.Controllers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    [TestCategory("App-Unit")]
    public class LibraryControllerUnitTests
    {
        [TestMethod]
        public async Task GetAllDataReturnsResultWhenModelStateContainsNoError()
        {

            //setup

            var handlerMock = new Mock<IGetAllDataHandler>();
            var responseMock = new Mock<IGetAllDataResponse>();
            var builderMock = new Mock<ILibraryAppResultBuilder>();
            var resultMock = new Mock<IActionResult>();
            var inputMock = new Mock<GetAllDataRequest>();

            var controllerMock = new Mock<LibraryController>() { CallBase = true };
            handlerMock.Setup(h => h.HandleAsync(It.IsAny<GetAllDataRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(responseMock.Object);
            builderMock.Setup(m => m.BuildClientResult<ILibraryResponse>(responseMock.Object, null)).Returns(resultMock.Object);

            var response = await controllerMock.Object.GetAsync(inputMock.Object, handlerMock.Object, builderMock.Object, CancellationToken.None);

            //verify
            Assert.IsNotNull(response);
            Assert.AreSame(response, resultMock.Object);

            handlerMock.Verify(
               h => h.HandleAsync(
                   It.IsAny<GetAllDataRequest>(), It.IsAny<CancellationToken>()), Times.Once);

        }


        [TestMethod]
        public async Task BorrowBookDataReturnsResultWhenModelStateContainsNoError()
        {
            var handlerMock = new Mock<IBorrowBookRequestHandler>();
            var responseMock = new Mock<IGetAllDataResponse>();
            var builderMock = new Mock<ILibraryAppResultBuilder>();
            var resultMock = new Mock<IActionResult>();
            var inputMock = new Mock<LibraryBorrowBookRequest>();

            var controllerMock = new Mock<LibraryController>() { CallBase = true };
            handlerMock.Setup(h => h.HandleAsync(It.IsAny<LibraryBorrowBookRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(responseMock.Object);
            builderMock.Setup(m => m.BuildClientResult<ILibraryResponse>(responseMock.Object, null)).Returns(resultMock.Object);

            var response = await controllerMock.Object.BorrowBookAsync(inputMock.Object, handlerMock.Object, builderMock.Object, CancellationToken.None);

            //verify
            Assert.IsNotNull(response);
            Assert.AreSame(response, resultMock.Object);

            handlerMock.Verify(
               h => h.HandleAsync(
                   It.IsAny<LibraryBorrowBookRequest>(), It.IsAny<CancellationToken>()), Times.Once);

        }


        [TestMethod]
        public async Task ReturnBookReturnsResultWhenModelStateContainsNoError()
        {
            var handlerMock = new Mock<IReturnBookRequestHandler>();
            var responseMock = new Mock<IGetAllDataResponse>();
            var builderMock = new Mock<ILibraryAppResultBuilder>();
            var resultMock = new Mock<IActionResult>();
            var inputMock = new Mock<ReturnBookRequest>();

            var controllerMock = new Mock<LibraryController>() { CallBase = true };
            handlerMock.Setup(h => h.HandleAsync(It.IsAny<ReturnBookRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(responseMock.Object);
            builderMock.Setup(m => m.BuildClientResult<ILibraryResponse>(responseMock.Object, null)).Returns(resultMock.Object);

            var response = await controllerMock.Object.ReturnBookAsync(inputMock.Object, handlerMock.Object, builderMock.Object, CancellationToken.None);

            //verify
            Assert.IsNotNull(response);
            Assert.AreSame(response, resultMock.Object);

            handlerMock.Verify(
               h => h.HandleAsync(
                   It.IsAny<ReturnBookRequest>(), It.IsAny<CancellationToken>()), Times.Once);

        }
    }
}
