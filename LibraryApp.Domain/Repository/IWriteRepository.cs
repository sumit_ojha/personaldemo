﻿using LibraryApp.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Domain.Repository
{
    public interface IWriteRepository<TConcrete> : IRepository where TConcrete : class, IAggregateRoot
    {

        Task<IEnumerable<TConcrete>> AddAsync(IEnumerable<TConcrete> entities, CancellationToken cancellationToken);

        //Task<TConcrete> AddOneRowAsync(TConcrete entities, CancellationToken cancellationToken);

        Task<IEnumerable<TConcrete>> UpdateAsync(IEnumerable<TConcrete> entities, CancellationToken cancellationToken);

        Task<IEnumerable<TConcrete>> DeleteAsync(IEnumerable<TConcrete> entities, CancellationToken cancellationToken);
    }
}
