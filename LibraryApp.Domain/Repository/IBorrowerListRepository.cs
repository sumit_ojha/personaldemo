﻿using LibraryApp.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Domain.Repository
{
    public interface IBorrowerListRepository : IReadRepository<IBorrowerList>, IWriteRepository<BorrowerList>
    {
        Task<List<BorrowerList>> GetBorrowerDetails(int borrowerId, CancellationToken cancellationToken);
    }
}
