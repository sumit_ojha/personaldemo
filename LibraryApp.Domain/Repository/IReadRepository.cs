﻿using LibraryApp.Domain.SeedWork;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Domain.Repository
{
    public interface IReadRepository<TEntity> where TEntity : IAggregateRoot
    {
        Task<IEnumerable<TEntity>> ListAllAsync(CancellationToken cancellationToken);
    }
}
