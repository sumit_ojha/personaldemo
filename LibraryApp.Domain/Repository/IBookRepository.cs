﻿using LibraryApp.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Domain.Repository
{
    public interface IBookRepository : IReadRepository<IBook>, IWriteRepository<Book>
    {
        Task<List<Book>> GetAllBookDetails();
    }
}
