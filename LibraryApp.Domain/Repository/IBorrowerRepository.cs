﻿using LibraryApp.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Domain.Repository
{
    public interface IBorrowerRepository : IReadRepository<IBorrower>, IWriteRepository<Borrower>
    {
        Task<Borrower> GetBorrowerById(int borrowerId, CancellationToken cancellationToken);
    }
}
