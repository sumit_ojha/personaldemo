﻿using LibraryApp.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.Domain.Models.Entities
{
    public interface IBook : IAggregateRoot
    {
       
        long Id { get; }
        string Name { get; }
        int Copies { get; }

    }
}
