﻿using LibraryApp.Domain.SeedWork;

namespace LibraryApp.Domain.Models.Entities
{
    public interface IBorrower : IAggregateRoot
    {
        long Id { get; }
        string BorrowerName { get; }
    }
}
