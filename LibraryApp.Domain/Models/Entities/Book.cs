﻿using System;

namespace LibraryApp.Domain.Models.Entities
{
    public class Book : IBook
    {
        public const string ArgumentsError = "Arguments cannot be Null";

        public long Id { get;  set; }

        public string Name { get;  set; }

        public int Copies { get;  set; }

        public class CreateBookArguments
        {
            public string Name { get; set; }
            public int Copies { get; set; }
           
        }

        public static Book Create(CreateBookArguments arguments)
        {
            ThrowIfInvalid(arguments, nameof(arguments), ArgumentsError);
            return new Book
            {
                Name = arguments.Name,
                Copies = arguments.Copies
            };
        }

        public static Book Update(Book response, CreateBookArguments arguments)
        {
            if (response == null) return null;
            response.Copies = arguments.Copies;
            return response;
        }

        private static void ThrowIfInvalid(object input, string paramName, string message)
        {
            if (input == null)
            {
                throw new ArgumentNullException(paramName, message);
            }
        }
    }
}
