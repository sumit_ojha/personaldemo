﻿using System;

namespace LibraryApp.Domain.Models.Entities
{
    public class Borrower : IBorrower
    {
        public const string ArgumentsError = "Arguments cannot be Null";

        public long Id { get; set; }
        public string BorrowerName { get; set; }
       


        public class CreateBorrowerArguments
        {
            public string BorrowerName { get; set; }
            
        }

        public static Borrower Create(CreateBorrowerArguments arguments)
        {
            ThrowIfInvalid(arguments, nameof(arguments), ArgumentsError);
            
            return new Borrower
            {
                BorrowerName = arguments.BorrowerName,
                
            };
        }

        public static Borrower Update(Borrower response, CreateBorrowerArguments arguments)
        {
            if (response == null) return null;
            response.BorrowerName = string.IsNullOrWhiteSpace(arguments.BorrowerName) ? response.BorrowerName : arguments.BorrowerName;
            
            return response;
        }

        private static void ThrowIfInvalid(object input, string paramName, string message)
        {
            if (input == null)
            {
                throw new ArgumentNullException(paramName, message);
            }
        }
    }
}
