﻿using LibraryApp.Domain.SeedWork;

namespace LibraryApp.Domain.Models.Entities
{
    public interface IBorrowerList : IAggregateRoot
    {
        long Id { get; }
        long BorrowerId { get; }
        long BookId { get; }
        bool Return { get; }
    }
}
