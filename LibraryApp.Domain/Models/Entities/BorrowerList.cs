﻿using System;

namespace LibraryApp.Domain.Models.Entities
{
    public class BorrowerList : IBorrowerList
    {
        public const string ArgumentsError = "Arguments cannot be Null";


        public long Id { get;  set; }

        public long BorrowerId { get;  set; }

        public long BookId { get;  set; }

        public bool Return { get;  set; }

        public class CreateBorrowerListArguments
        {
            public long BorrowerId { get; set; }
            public long BookId { get; set; }
            public bool Return { get; set; }
        }

        public static BorrowerList Create(CreateBorrowerListArguments arguments)
        {
            ThrowIfInvalid(arguments, nameof(arguments), ArgumentsError);

            return new BorrowerList
            {
                BorrowerId = arguments.BorrowerId,
                BookId = arguments.BookId,
                Return = arguments.Return
            };
        }

        public static BorrowerList Update(BorrowerList response, CreateBorrowerListArguments arguments)
        {
            if (response == null) return null;
            response.Return = arguments.Return;


            return response;
        }

        private static void ThrowIfInvalid(object input, string paramName, string message)
        {
            if (input == null)
            {
                throw new ArgumentNullException(paramName, message);
            }
        }
    }
}
