﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.Domain.SeedWork
{
    public interface IRepository
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
