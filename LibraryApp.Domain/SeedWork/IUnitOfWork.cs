﻿using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Domain.SeedWork
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
