﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LibraryApp.Domain.Converters
{
    /// <summary>
    /// Converter to instantiate concrete type {TC} derived from {TI} interface
    /// </summary>
    /// <typeparam name="TI">The type of the i.</typeparam>
    /// <typeparam name="TC">The type of the c.</typeparam>
    /// <seealso cref="Newtonsoft.Json.Converters.CustomCreationConverter{TI}"/>
    public class InterfaceTypeConverter<TI, TC> : CustomCreationConverter<TI> where TC : TI, new()
    {
        /// <inheritdoc/>
        public override TI Create(Type objectType)
        {
            return new TC();
        }
    }

    public class MicrosecondEpochConverter : DateTimeConverterBase
    {
        private static readonly DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue(((DateTime)value - _epoch).TotalMilliseconds + "000");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) { return null; }
            return _epoch.AddMilliseconds((long)reader.Value / 1000d);
        }
    }
}